#! /usr/bin/env python

import tweepy
from tweepy import OAuthHandler
import sys
import os
import jsonpickle
from credentials import *

#  auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
#  auth.set_access_token(access_token, access_secret)

#  api = tweepy.API(auth)
#  places = api.geo_search(query="México", granularity="city")

#  place_id = places[2].id
#  print('El id de la Ciudad de Mexico es: ',place_id)

auth = tweepy.AppAuthHandler(consumer_key, consumer_secret)
api = tweepy.API(auth, wait_on_rate_limit=True,wait_on_rate_limit_notify=True)

searchQuery = 'place:1808e9ed9e667620'

maxTweets = 1000000000
tweetsPerQry = 100

tweetCount = 0
print("Descargando max{0} tweets".format(maxTweets))

f = open('HistoricoSismo.json', 'a')
while tweetCount < maxTweets:
    try:
        for tweet in tweepy.Cursor(api.search,q=searchQuery).items(maxTweets) :
            if tweet.place is not None:
                f.write(jsonpickle.encode(tweet._json, unpicklable=False) + '\n')
                tweetCount += 1
    except tweepy.TweepError as e:
        print("Algún error ocurrió: " + str(e))
        break
f.close()

print("Llevo {0} tweets".format(tweetCount))
