#! /usr/bin/env python

import tweepy
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import json
from credentials import *

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)

api = tweepy.API(auth,wait_on_rate_limit=True,wait_on_rate_limit_notify=True)

class ListenerSismo(StreamListener):

    def on_data(self, data):
        try:
            with open('sismodata.json', 'a') as f:
                f.write(data)
                return True
        except BaseException as e:
                print('&quot;Error on_data: %s&quot; % str(e)')
        return True

    def on_errors(self, status):
        print(status)
        return True

twitter_stream = Stream(auth, ListenerSismo())
twitter_stream.filter(locations=[-99.3687,19.2222,-98.4062,19.6001])
